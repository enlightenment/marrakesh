#ifndef MRKLIB_H
#define MRKLIB_H

#include <Eina.h>

EAPI Eina_Bool   mrk_init(void);
EAPI void        mrk_shutdown(void);
EAPI const char *mrk_arch_get(void);
EAPI const char *mrk_os_get(void);

// yes - this should become an eo object - later. right now... cleaning up
// the mess that was putting the pipeline together

// mrk client api - for clients that conne3ct to an mrk server to upload,
// download, query etc. etc. the server - so the "main" set of api for a
// client-server api. in future protocol should be extendable in api
// below with sending extended requests and handling return data
typedef struct _Mrk Mrk;

EAPI Mrk        *mrk_connect(const char *host, int port);
EAPI void        mrk_disconnect(Mrk *server);
EAPI Eina_Bool   mrk_upload(Mrk *server, const char *file);
EAPI Eina_Bool   mrk_download(Mrk *server, const char *package);
EAPI Eina_Bool   mrk_download_source(Mrk *server, const char *package);
EAPI Eina_Bool   mrk_list(Mrk *server, const char *category);
EAPI Eina_Bool   mrk_search(Mrk *server, const char *keys);
EAPI Eina_Bool   mrk_getkey(Mrk *server);

EAPI void        mrk_callback_connect_set(Mrk *server,
                                          void (*connect) (void *data, Mrk *server),
                                          void *connect_data,
                                          void (*disconnect) (void *data, Mrk *server),
                                          void *disconnect_data);
EAPI void        mrk_callback_upload_set(Mrk *server,
                                         void (*upload_begin) (void *data, Mrk *server),
                                         void *upload_begin_data,
                                         void (*upload_progress) (void *data, Mrk *server, double pos),
                                         void *upload_progress_data,
                                         void (*upload_end) (void *data, Mrk *server),
                                         void *upload_end_data,
                                         void (*upload_success) (void *data, Mrk *server, Eina_Bool sucess),
                                         void *upload_success_data);
EAPI void        mrk_callback_download_set(Mrk *server,
                                           void (*download_begin) (void *data, Mrk *server),
                                           void *download_begin_data,
                                           void (*download_progress) (void *data, Mrk *server, double pos),
                                           void *download_progress_data,
                                           void (*download_end) (void *data, Mrk *server),
                                           void *download_end_data,
                                           void (*download_success) (void *data, Mrk *server, const char *file),
                                           void *download_success_data);
EAPI void        mrk_callback_answer_set(Mrk *server,
                                         void (*answer_begin) (void *data, Mrk *server),
                                         void *answer_begin_data,
                                         void (*answer_progress) (void *data, Mrk *server, const char *result),
                                         void *answer_progress_data,
                                         void (*answer_end) (void *data, Mrk *server),
                                         void *answer_end_data);
EAPI void        mrk_callback_getkey_set(Mrk *server,
                                         void (*getkey) (void *data, Mrk *server, const char *answer),
                                         void *getkey_data);

// api to run a mrk server - right now you have very little choice here
// as everything is just magically handled internally - in future some way
// to access the repodb that a serve loads (and auto-updates) as well as
// get callbacks when things happen and also to extend protocol
typedef struct _Mrk_Serve Mrk_Serve;

EAPI Mrk_Serve *mrk_serve(const char *listen, int port, const char *repodir, const char *bldsh);
EAPI void       mrk_unserve(Mrk_Serve *server);

// this is the api to deal with a repository database of files and to
// request and update of them, auto-reload when update done and tell you
// when it's done
typedef struct _Mrk_Repodb Mrk_Repodb;

EAPI Mrk_Repodb      *mrk_repodb_load(const char *repodir);
EAPI void             mrk_repodb_free(Mrk_Repodb *rdb);
EAPI void             mrk_repodb_callback_update_set(Mrk_Repodb *rdb,
                                                     void (*update) (void *data, Mrk_Repodb *rdb),
                                                     void *update_data);
EAPI void             mrk_repodb_update(Mrk_Repodb *rdb);
EAPI const Eina_List *mrk_repodb_category_list(Mrk_Repodb *rdb,
                                               const char *arch,
                                               const char *category);
EAPI Eina_List       *mrk_repodb_search(Mrk_Repodb *rdb,
                                        const char *arch,
                                        const char *search);
// this iapi si very simple - it scans the dir of mrk binary packages and
// updates the database files inside
EAPI Eina_Bool        mrk_index(const char *dir);

typedef struct _Mrk_Build      Mrk_Build;
typedef struct _Mrk_Build_Bin  Mrk_Build_Bin;
typedef struct _Mrk_Build_Data Mrk_Build_Data;

struct _Mrk_Build_Bin
{
   const char *bin;
   Eina_List *srcs;
   Eina_List *deps;
   Eina_List *incs;
};

struct _Mrk_Build_Data
{
   const char *src;
   const char *dest;
};

struct _Mrk_Build
{
   const char *name;
   const char *icon;
   const char *splash;
   const char *brief;
   const char *version;
   const char *license;
   const char *domain;
   const char *repo;
   const char *devrepo;
   const char *contact;
   Eina_List *tags;
   Eina_List *categories;
   Eina_List *copying;
   const char *needs;
   Eina_List *bins;
   Eina_List *data;
   Eina_List *desktops;
   Eina_List *icons;
   Eina_List *po;
};

EAPI Mrk_Build *mrk_build_load(const char *file);
EAPI void       mrk_build_free(Mrk_Build *bld);
EAPI Eina_Bool  mrk_build_do(Mrk_Build *bld, const char *tmpd, const char *objd);
EAPI Eina_Bool  mrk_build_package_bin(Mrk_Build *bld, const char *file, const char *tmpd, const char *os, const char *arch, const char *key_cert_file, const char *key_priv_file);
EAPI Eina_Bool  mrk_build_package_src(Mrk_Build *bld, const char *buildfile, const char *file, const char *key_cert_file, const char *key_priv_file);

EAPI Eina_Bool  mrk_package_src_extract(const char *file, const char *dst);
EAPI Eina_Bool  mrk_package_bin_clean(void);
EAPI Eina_Bool  mrk_package_verify(const char *file, const char *key_cert_file);
EAPI Eina_Bool  mrk_package_bin_install(const char *file, const char *os, const char *arch);
EAPI Eina_Bool  mrk_package_bin_remove(const char *name);

// XXX: .mrk parsing api handled/exposed and other related functions
// XXX: installation/uninstallation handling apis
// XXX: cleanup apis

#endif
