#include "mrklib_priv.h"

const char    *_mrk_server_host = MRK_SERVER_HOST;
int            _mrk_server_port = MRK_SERVER_PORT;
const char    *_mrk_appdir      = NULL;
const char    *_mrk_appdir_tmp  = NULL;
const char    *_mrk_appdir_bin  = NULL;
unsigned char *_mrk_uuid        = NULL;
int            _mrk_uuid_len    = 0;

static int _mrk_init = 0;

static void
_entropy_get(unsigned char *dat, int len)
{
   int fd;
   Eina_Bool ok = EINA_FALSE;

   fd = open("/dev/random", O_RDONLY);
   if (fd >= 0)
     {
        if (read(fd, dat, len) == len) ok = EINA_TRUE;
        close(fd);
     }
   if (!ok)
     {
        struct timeval tv;
        unsigned int seed = 0x12345678;
        int i;

        if (gettimeofday(&tv, NULL) == 0)
          seed = ((tv.tv_sec << 16) | (tv.tv_sec >> 16)) ^ tv.tv_usec;
        else
          seed = (unsigned int)time(NULL);
        srand(seed);
        for (i = 0; i < len; i++) dat[i] = rand() & 0xff;
     }
}

static void
_mrk_config_new(const char *file)
{
   Eet_File *ef;
   unsigned char uuid[32];
   int uuid_len = sizeof(uuid);

   ef = eet_open(file, EET_FILE_MODE_WRITE);
   if (!ef) return;
   _entropy_get(uuid, uuid_len);
   eet_write(ef, "uuid", uuid, uuid_len, EET_COMPRESSION_VERYFAST);
   eet_close(ef);
}

static void
_mrk_config_init(const char *file)
{
   Eet_File *ef;
   int len;

   ef = eet_open(file, EET_FILE_MODE_READ);
   if (!ef)
     {
        _mrk_config_new(file);
        ef = eet_open(file, EET_FILE_MODE_READ);
     }
   if (!ef) return;
   _mrk_uuid = eet_read(ef, "uuid", &len);
   if ((!_mrk_uuid) || (len != 32))
     {
        free(_mrk_uuid);
        _mrk_uuid = NULL;
        eet_close(ef);
        _mrk_config_new(file);
        ef = eet_open(file, EET_FILE_MODE_READ);
        if (!ef) return;
        _mrk_uuid = eet_read(ef, "uuid", &len);
        if ((!_mrk_uuid) || (len != 32))
          {
             free(_mrk_uuid);
             _mrk_uuid = NULL;
             eet_close(ef);
             return;
          }
     }
   _mrk_uuid_len = len;
   eet_close(ef);
}

static void
_mrk_appdir_init(void)
{
   char tmp[4096];
   const char *home;

   home = getenv("XDG_DATA_HOME");
   if (!home) home = getenv("HOME");
   if (!home) home = "/tmp";

   snprintf(tmp, sizeof(tmp), "%s/Applications", home);
   if (!ecore_file_exists(tmp)) ecore_file_mkdir(tmp);
   _mrk_appdir = eina_stringshare_add(tmp);

   snprintf(tmp, sizeof(tmp), "%s/Applications/.bin", home);
   if (!ecore_file_exists(tmp)) ecore_file_mkdir(tmp);
   _mrk_appdir_bin = eina_stringshare_add(tmp);

   snprintf(tmp, sizeof(tmp), "%s/Applications/.tmp", home);
   if (!ecore_file_exists(tmp)) ecore_file_mkdir(tmp);
   _mrk_appdir_tmp = eina_stringshare_add(tmp);

   snprintf(tmp, sizeof(tmp), "%s/Applications/.marrakesh.cfg", home);
   _mrk_config_init(tmp);
}

EAPI Eina_Bool
mrk_init(void)
{
   _mrk_init++;
   if (_mrk_init == 1)
     {
        eina_init();
        eet_init();
        ecore_init();
        ecore_ipc_init();
        ecore_file_init();
        _mrk_appdir_init();
     }
   return EINA_TRUE;
}

EAPI void
mrk_shutdown(void)
{
   _mrk_init--;
   if (_mrk_init == 0)
     {
        eina_stringshare_del(_mrk_appdir);
        _mrk_appdir = NULL;
        eina_stringshare_del(_mrk_appdir_bin);
        _mrk_appdir_bin = NULL;
        eina_stringshare_del(_mrk_appdir_tmp);
        _mrk_appdir_tmp = NULL;
        ecore_file_shutdown();
        ecore_ipc_shutdown();
        eet_shutdown();
        ecore_shutdown();
        eina_shutdown();
     }
}

EAPI const char *
mrk_arch_get(void)
{
   return ARCH;
}

EAPI const char *
mrk_os_get(void)
{
   return OS;
}

