#include "mrklib.h"

#include <Ecore.h>
#include <Ecore_File.h>
#include <Ecore_Ipc.h>
#include <Eet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>

#define MRK_SERVER_HOST "devs.enlightenment.org"
#define MRK_SERVER_PORT 10077

extern const char    *_mrk_server_host;
extern int            _mrk_server_port;
extern const char    *_mrk_appdir;
extern const char    *_mrk_appdir_tmp;
extern const char    *_mrk_appdir_bin;
extern unsigned char *_mrk_uuid;
extern int            _mrk_uuid_len;

#define M_OP         10

#define M_UP_B       0
#define M_UP_E       10
#define M_UP_START   1
#define M_UP_DATA    2
#define M_UP_END     3
#define M_UP_OK      4
#define M_UP_FAIL    5

#define M_DOWN_B     10
#define M_DOWN_E     20
#define M_DOWN_START 11
#define M_DOWN_DATA  12
#define M_DOWN_END   13

#define M_QRY_B      20
#define M_QRY_E      30
#define M_QRY_LIST   21
#define M_QRY_SEARCH 22
#define M_QRY_GET    23
#define M_QRY_GETSRC 24

#define M_ANS_B      30
#define M_ANS_E      40
#define M_ANS_START  31
#define M_ANS_DATA   32
#define M_ANS_END    33

#define M_SRC_B      40
#define M_SRC_E      50
#define M_SRC_START  41
#define M_SRC_DATA   42
#define M_SRC_END    43

#define M_ID_B       50
#define M_ID_E       60
#define M_ID_UUID    51
#define M_ID_VERSION 52
#define M_ID_ARCH    53

#define M_INF_B      60
#define M_INF_E      70
#define M_INF_START  61
#define M_INF_DATA   62
#define M_INF_END    63
#define M_INF_GET_IC 64
#define M_INF_GET_SP 65
#define M_INF_GET_IN 66

#define M_GETKEY_B   70
#define M_GETKEY_E   80
#define M_GETKEY_REQ 71
#define M_GETKEY_ANS 72


#if      defined(__x86_64__)
# define ARCH "x86_64"
#elif    defined(__i386__)
# define ARCH "ix86"
#elif    defined(__arm__)
# ifdef __ARM_V7__
#  define ARCH "armv7"
# else
#  define ARCH "armv6"
# endif
#elif    defined(__ppc64__) || defined(__powerpc64__)
# define ARCH "ppc64"
#elif    defined(__ppc__) || defined(__powerpc__)
# define ARCH "ppc"
#endif

#if      defined(__linux__)
# define OS "linux"
#elif    defined(__MACH__)
# define OS "osx"
#elif    defined(__FreeBSD__)
# define OS "freebsd"
#elif    defined(_WIN32) || defined(_WIN64)
# define OS "win"
#endif

Eina_Bool  _mrk_util_plain_file_check(const char *file);
Eina_Bool  _mrk_util_plain_path_check(const char *file);
char      *_mrk_util_proto_string(Ecore_Ipc_Event_Server_Data *e);
char      *_mrk_util_proto_cli_string(Ecore_Ipc_Event_Client_Data *e);
Eina_Bool  _mrk_util_arch_ok(const char *arch);
