#include <Eina.h>
#include <Ecore.h>
#include <Eet.h>
#include <Ecore_Ipc.h>
#include <Ecore_File.h>

#include "mrk-proto.h"
#include "mrk-db.h"

static const char *sane_name_veto[] = {"../", "./", "/", NULL};
static const char *sane_name_ok = "01234567890-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.";

static char *repodir = "./repo";
static char *build_sh = "./mrk-srv-build.sh";

typedef struct
{
   const char *arch; // also dir
   Eina_Hash *name_hash;
   Eina_Hash *category_hash;
   Eina_Hash *tag_hash;
   Eina_Hash *key_hash;
} Db;

static Eina_List *dbs = NULL;

static void
dbs_load_hash(Eina_Hash *hash, FILE *f)
{
   char line[16384], *s;
   int len;
   Eina_List *list = NULL;
   const char *key = NULL;

   for (;;)
     {
        s = fgets(line, sizeof(line), f);
        if (!s) break;;
        len = strlen(s);
        if (len < 1) continue;
        s[len - 1] = 0;
        if ((s[0] == '\t') && (key))
          {
             list = eina_list_append(list, eina_stringshare_add(s + 1));
          }
        else
          {
             if (key) eina_hash_set(hash, key, list);
             list = NULL;
             if (key) eina_stringshare_del(key);
             key = eina_stringshare_add(s);
          }
     }
   if (key) eina_hash_set(hash, key, list);
   list = NULL;
   if (key) eina_stringshare_del(key);
   key = NULL;
}

static void
dbs_load(void)
{
   Eina_List *files;
   char *s;
   char tmp[4096];

   files = ecore_file_ls(repodir);
   EINA_LIST_FREE(files, s)
     {
        Db *db = NULL;
        FILE *f;

        if (s[0] == '.')
          {
             free(s);
             continue;
          }
        snprintf(tmp, sizeof(tmp), "%s/%s/.mrkdb-name.txt", repodir, s);
        f = fopen(tmp, "rb");
        if (f)
          {
             db = calloc(1, sizeof(Db));
             if (db)
               {
                  db->arch = strdup(s);

                  if (db->arch)
                    {
                       db->name_hash = eina_hash_string_superfast_new(NULL);
                       db->category_hash = eina_hash_string_superfast_new(NULL);
                       db->tag_hash = eina_hash_string_superfast_new(NULL);
                       db->key_hash = eina_hash_string_superfast_new(NULL);
                       dbs = eina_list_append(dbs, db);
                       dbs_load_hash(db->name_hash, f);
                    }
                  else
                    {
                       free(db);
                       db = NULL;
                    }
               }
             fclose(f);
          }
        if (db)
          {
             snprintf(tmp, sizeof(tmp), "%s/%s/.mrkdb-tag.txt", repodir, s);
             f = fopen(tmp, "rb");
             if (f)
               {
                  dbs_load_hash(db->tag_hash, f);
                  fclose(f);
               }
             snprintf(tmp, sizeof(tmp), "%s/%s/.mrkdb-category.txt", repodir, s);
             f = fopen(tmp, "rb");
             if (f)
               {
                  dbs_load_hash(db->category_hash, f);
                  fclose(f);
               }
             snprintf(tmp, sizeof(tmp), "%s/%s/.mrkdb-key.txt", repodir, s);
             f = fopen(tmp, "rb");
             if (f)
               {
                  dbs_load_hash(db->key_hash, f);
                  fclose(f);
               }
          }
        db = NULL;
     }
}

static Eina_Bool
dbs_clear_hash_cb(const Eina_Hash *hash, const void *key, void *data, void *fdata)
{
   const char *s;

   EINA_LIST_FREE(data, s)
     {
        eina_stringshare_del(s);
     }
   return EINA_TRUE;
}

static void
dbs_clear_hash(Eina_Hash *hash)
{
   if (!hash) return;
   eina_hash_foreach(hash, dbs_clear_hash_cb, NULL);
   eina_hash_free(hash);
}

static void
dbs_clear(void)
{
   Db *db;

   EINA_LIST_FREE(dbs, db)
     {
        dbs_clear_hash(db->name_hash);
        dbs_clear_hash(db->category_hash);
        dbs_clear_hash(db->tag_hash);
        dbs_clear_hash(db->key_hash);
        free(db);
     }
}

static Ecore_Exe *dbexe = NULL;
static int db_update = 0;

static void
dbs_update(void)
{
   Eina_List *files;
   char *s;
   char tmp[4096];

   if (dbexe)
     {
        db_update = 1;
        return;
     }
   snprintf(tmp, sizeof(tmp), "mrk-index %s/*-*", repodir);
   dbexe = ecore_exe_pipe_run(tmp, 0, NULL);
   printf("dbs update: run: %s\n", tmp);
}

static void
dbs_update_final(void)
{
   printf("dbs update final\n");
   dbs_clear();
   dbs_load();
}

static Db *
dbs_arch_find(const char *arch)
{
   Eina_List *l;
   Db *db;

   if (!arch) return NULL;
   EINA_LIST_FOREACH(dbs, l, db)
     {
        if (!strcmp(db->arch, arch))
          {
             if (dbs != l) dbs = eina_list_promote_list(dbs, l);
             return db;
          }
     }
   return NULL;
}

static int
sane_forbidden_path(const char *file, const char **forbidden)
{
   int i;

   for (i = 0; forbidden[i]; i++)
     {
        if (strstr(forbidden[i], file)) return 0;
     }
   return 1;
}

static int
sane_allowed_path(const char *file, const char *allowed)
{
   int i, j;

   for (i = 0; file[i]; i++)
     {
        int ok;

        ok = 0;
        for (j = 0; allowed[j]; j++)
          {
             if (file[i] == allowed[j])
               {
                  ok = 1;
                  break;
               }
          }
        if (!ok) return 0;
     }
   return 1;
}

typedef struct
{
   Ecore_Ipc_Client *client;
   char *file;
   char *dir;
   FILE *f;
   Ecore_Exe *exe;
   char *arch;
   char *version;
} Client;

static Ecore_Ipc_Server *ipc = NULL;
static Eina_List *clients = NULL;

static Client *
client_find(Ecore_Ipc_Client *ipc_client)
{
   Eina_List *l;
   Client *c;

   EINA_LIST_FOREACH(clients, l, c)
     {
        if (c->client == ipc_client) return c;
     }
   return NULL;
}

static Client *
client_exe_find(Ecore_Exe *exe)
{
   Eina_List *l;
   Client *c;

   EINA_LIST_FOREACH(clients, l, c)
     {
        if (c->exe == exe) return c;
     }
   return NULL;
}

static Client *
client_new(void)
{
   Client *c;

   c = calloc(1, sizeof(Client));
   clients = eina_list_append(clients, c);
   return c;
}

static void
client_free(Client *c)
{
   if (c->f) fclose(c->f);
   if (c->file) free(c->file);
   if (c->dir)
     {
        ecore_file_recursive_rm(c->dir);
        free(c->dir);
     }
   clients = eina_list_remove(clients, c);
   free(c);
}

static void
client_upload_new(Client *c)
{
   char tmp[4096];
   Eina_Tmpstr *s = NULL;

   if (c->f) fclose(c->f);
   if (c->dir)
     {
        ecore_file_recursive_rm(c->dir);
        free(c->dir);
        c->dir = NULL;
     }
   if (eina_file_mkdtemp("marrekesh-up-XXXXXX", &s))
     {
        c->dir = strdup(s);
        eina_tmpstr_del(s);
        if (c->dir)
          {
             snprintf(tmp, sizeof(tmp), "%s/%s", c->dir, c->file);
             c->f = fopen(tmp, "wb");
          }
     }
}

static void
client_upload_end(Client *c)
{
   char tmp[4096];

   if (c->f)
     {
        fclose(c->f);
        c->f = NULL;
        snprintf(tmp, sizeof(tmp), "%s %s %s", build_sh, c->dir, repodir);
        c->exe = ecore_exe_pipe_run(tmp,
                                    ECORE_EXE_TERM_WITH_PARENT |
                                    ECORE_EXE_NOT_LEADER, c);
     }
}

static void
client_upload_fail(Client *c)
{
   if (c->f) fclose(c->f);
   if (c->file) free(c->file);
   if (c->dir)
     {
        ecore_file_recursive_rm(c->dir);
        free(c->dir);
     }
   c->f = NULL;
   c->file = NULL;
   c->dir = NULL;
}

static void
client_upload_data(Client *c, void *data, int size)
{
   if (c->f) fwrite(data, size, 1, c->f);
}

static void
client_send(Client *c, const char *name)
{
   char tmp[4096];
   char *lnk;

   printf("C arch: %s\n", c->arch);
   if (!c->arch) return;
   snprintf(tmp, sizeof(tmp), "%s/%s/%s", repodir, c->arch, name);
   lnk = ecore_file_readlink(tmp);
   if (!lnk)
     ecore_ipc_client_send(c->client, 10, M_DOWN_START, 0, 0, 0, NULL, 0);
   else
     {
        FILE *f;

        f = fopen(tmp, "rb");
        if (!f)
          ecore_ipc_client_send(c->client, 10, M_DOWN_START, 0, 0, 0, NULL, 0);
        else
          {
             ecore_ipc_client_send(c->client, 10, M_DOWN_START, 0, 0, 0, lnk, strlen(lnk));
             for (;;)
               {
                  size_t size;
                  char buf[10000];

                  size = fread(buf, 1, 10000, f);
                  if (size <= 0) break;
                  ecore_ipc_client_send(c->client, 10, M_DOWN_DATA, 0, 0, 0, buf, size);
               }
             ecore_ipc_client_send(c->client, 10, M_DOWN_END, 0, 0, 0, NULL, 0);
             fclose(f);
          }
     }
}

static void
client_src_send(Client *c, const char *name)
{
   char tmp[4096];
   char *lnk;

   snprintf(tmp, sizeof(tmp), "%s/src/%s", repodir, name);
   lnk = ecore_file_readlink(tmp);
   if (!lnk)
     ecore_ipc_client_send(c->client, 10, M_SRC_START, 0, 0, 0, NULL, 0);
   else
     {
        FILE *f;

        f = fopen(tmp, "rb");
        if (!f)
          ecore_ipc_client_send(c->client, 10, M_SRC_START, 0, 0, 0, NULL, 0);
        else
          {
             ecore_ipc_client_send(c->client, 10, M_SRC_START, 0, 0, 0, lnk, strlen(lnk));
             for (;;)
               {
                  size_t size;
                  char buf[10000];

                  size = fread(buf, 1, 10000, f);
                  if (size <= 0) break;
                  ecore_ipc_client_send(c->client, 10, M_SRC_DATA, 0, 0, 0, buf, size);
               }
             ecore_ipc_client_send(c->client, 10, M_SRC_END, 0, 0, 0, NULL, 0);
             fclose(f);
          }
     }
}

static int
arch_ok(const char *arch)
{
   if (!strcmp(arch, "linux-x86_64")) return 1;
   if (!strcmp(arch, "linux-ix86")) return 1;
   if (!strcmp(arch, "linux-arm")) return 1;
   if (!strcmp(arch, "linux-ppc64")) return 1;
   if (!strcmp(arch, "linux-ppc")) return 1;
   return 0;
}

static int
version_ok(const char *arch)
{
   return 1;
}

static Eina_List *
search_intersect(Eina_List *list_in, Eina_List *list2)
{
   Eina_List *list = NULL, *ll;
   const char *s, *ss;
   int ok;

   EINA_LIST_FREE(list_in, s)
     {
        ok = 0;
        EINA_LIST_FOREACH(list2, ll, ss)
          {
             if (ss == s)
               {
                  ok = 1;
                  break;
               }
          }
        if (ok) list = eina_list_append(list, s);
        else eina_stringshare_del(s);
     }
   return list;
}

static Eina_List *
search_build(Eina_Hash *hash, const char *search)
{
   Eina_List *list = NULL, *tl, *l;
   const char *s;
   char *ts = strdup(search);
   char *p, *end;
   int i;

   if (!ts) return NULL;
   for (p = ts; *p; p++)
     {
        if (isspace(*p)) *p = 0;
        end = p;
     }
   for (p = ts; p < end; p++)
     {
        if (!*p) continue;
        tl = eina_hash_find(hash, p);
        if (p == ts)
          {
             EINA_LIST_FOREACH(tl, l, s)
               list = eina_list_append(list, eina_stringshare_add(s));
          }
        else
          {
             list = search_intersect(list, tl);
          }
        p += strlen(p);
     }
   free(ts);
   return list;
}

static Eina_Bool
_ipc_cb_add(void *data, int type, void *event)
{
   Ecore_Ipc_Event_Client_Add *e = event;
   Client *c = client_new();
   c->client = e->client;
   return EINA_TRUE;
}

static Eina_Bool
_ipc_cb_del(void *data, int type, void *event)
{
   Ecore_Ipc_Event_Client_Del *e = event;
   Client *c = client_find(e->client);
   if (c) client_free(c);
   return EINA_TRUE;
}

static Eina_Bool
_ipc_cb_dat(void *data, int type, void *event)
{
   Ecore_Ipc_Event_Client_Data *e = event;
   Client *c = client_find(e->client);
   // e->major e->minor e->ref e->ref_to e->response | e->data e->size
   if (e->major != 10) return EINA_TRUE;
   switch (e->minor)
     {
      case M_UP_START:
        // e->data == filename
        if ((e->size > 0) && (e->size < 1000))
          {
             if (c->file) free(c->file);
             c->file = malloc(e->size + 1);
             memcpy(c->file, e->data, e->size);
             c->file[e->size] = 0;
             if (sane_forbidden_path(c->file, sane_name_veto) &&
                 sane_allowed_path(c->file, sane_name_ok))
               {
                  client_upload_new(c);
               }
             else client_upload_fail(c);
          }
        else client_upload_fail(c);
        break;
      case M_UP_DATA:
        // e->data == chunk of bytes to append
        if ((e->size > 0) && (e->size <= 10000))
          {
             client_upload_data(c, e->data, e->size);
          }
        else client_upload_fail(c);
        break;
      case M_UP_END:
        // e->data == ignored (EOF)
        client_upload_end(c);
        break;
      case M_QRY_LIST:
        // e->data == category (none if all)
        ecore_ipc_client_send(c->client, 10, M_ANS_START, 0, 0, 0, NULL, 0);
        if ((e->size > 0) && (e->size < 1000))
          {
             Eina_List *list = NULL, *l;
             const char *s;
             Db *db = dbs_arch_find(c->arch);

             if (db)
               {
                  char *str = malloc(e->size + 1);
                  if (str)
                    {
                       memcpy(str, e->data, e->size);
                       str[e->size] = 0;
                       list = eina_hash_find(db->category_hash, str);
                       free(str);
                       EINA_LIST_FOREACH(list, l, s)
                         {
                            ecore_ipc_client_send(c->client, 10, M_ANS_DATA, 0, 0, 0, s, strlen(s));
                         }
                    }
               }
          }
        else
          {
             Eina_List *list = NULL, *l;
             const char *s;
             Db *db = dbs_arch_find(c->arch);

             if (db)
               {
                  list = eina_hash_find(db->category_hash, "@");
                  EINA_LIST_FOREACH(list, l, s)
                    {
                       ecore_ipc_client_send(c->client, 10, M_ANS_DATA, 0, 0, 0, s, strlen(s));
                    }
               }
          }
        ecore_ipc_client_send(c->client, 10, M_ANS_END, 0, 0, 0, NULL, 0);
        break;
      case M_QRY_SEARCH:
        // e->data == query string
        ecore_ipc_client_send(c->client, 10, M_ANS_START, 0, 0, 0, NULL, 0);
        if ((e->size > 0) && (e->size < 1000))
          {
             Eina_List *list = NULL;
             const char *s;
             Db *db = dbs_arch_find(c->arch);

             if (db)
               {
                  char *str = malloc(e->size + 1);
                  if (str)
                    {
                       memcpy(str, e->data, e->size);
                       str[e->size] = 0;
                       list = search_build(db->key_hash, str);
                       free(str);
                       EINA_LIST_FREE(list, s)
                         {
                            ecore_ipc_client_send(c->client, 10, M_ANS_DATA, 0, 0, 0, s, strlen(s));
                            eina_stringshare_del(s);
                         }
                    }
               }
          }
        ecore_ipc_client_send(c->client, 10, M_ANS_END, 0, 0, 0, NULL, 0);
        break;
      case M_QRY_GET:
        if ((e->size > 0) && (e->size < 1000))
          {
             char *name = malloc(e->size + 1);
             if (name)
               {
                  memcpy(name, e->data, e->size);
                  name[e->size] = 0;
                  client_send(c, name);
                  free(name);
               }
          }
        // e->data == appname (Terminology, Rage etc.)
        break;
      case M_QRY_GETSRC:
        if ((e->size > 0) && (e->size < 1000))
          {
             char *name = malloc(e->size + 1);
             if (name)
               {
                  memcpy(name, e->data, e->size);
                  name[e->size] = 0;
                  client_src_send(c, name);
                  free(name);
               }
          }
        // e->data == appname (Terminology, Rage etc.)
        break;
      case M_ID_UUID:
        break;
      case M_ID_VERSION:
        if ((e->size > 0) && (e->size < 1000))
          {
             char *version = malloc(e->size + 1);
             if (version)
               {
                  memcpy(version, e->data, e->size);
                  version[e->size] = 0;
                  if (version_ok(version))
                    {
                       if (c->version) free(c->version);
                       c->version = version;
                    }
                  else free(version);
               }
          }
        break;
      case M_ID_ARCH:
        if ((e->size > 0) && (e->size < 1000))
          {
             char *arch = malloc(e->size + 1);
             if (arch)
               {
                  memcpy(arch, e->data, e->size);
                  arch[e->size] = 0;
                  if (arch_ok(arch))
                    {
                       if (c->arch) free(c->arch);
                       c->arch = arch;
                    }
                  else free(arch);
               }
          }
        break;
      default:
        break;
     }

   return EINA_TRUE;
}

static Eina_Bool
_exe_cb_del(void *data, int type, void *event)
{
   Ecore_Exe_Event_Del *ev = event;
   Client *c = client_exe_find(ev->exe);
   if (c)
     {
        if (ev->exit_code == 0)
          {
             ecore_ipc_client_send(c->client, 10, M_UP_OK, 0, 0, 0, NULL, 0);
             dbs_update();
          }
        else
          ecore_ipc_client_send(c->client, 10, M_UP_FAIL, 0, 0, 0, NULL, 0);
        c->exe = NULL;
     }
   else if (ev->exe == dbexe)
     {
        dbexe = NULL;
        // we had a pending db update - redo it
        if (db_update)
          {
             db_update = 0;
             dbs_update_final();
             dbs_update();
          }
        else dbs_update_final();
     }
   return EINA_TRUE;
}

static int
ipc_init(void)
{
   ipc = ecore_ipc_server_add(ECORE_IPC_REMOTE_SYSTEM, "0.0.0.0", 10077, NULL);
   if (!ipc) return 0;
   ecore_event_handler_add(ECORE_IPC_EVENT_CLIENT_ADD,  _ipc_cb_add, NULL);
   ecore_event_handler_add(ECORE_IPC_EVENT_CLIENT_DEL,  _ipc_cb_del, NULL);
   ecore_event_handler_add(ECORE_IPC_EVENT_CLIENT_DATA, _ipc_cb_dat, NULL);
   return 1;
}

int
main(int argc, char **argv)
{
   if (argc < 2)
     {
        printf("usage:\n"
               "  mrk-srv [REPO_DIR] [BUILD_SCRIPT]\n"
               "\n");
        exit(1);
     }
   eina_init();
   ecore_init();
   eet_init();
   ecore_file_init();
   ecore_ipc_init();

   if (argc > 1) repodir = ecore_file_realpath(argv[1]);
   if (argc > 2) build_sh = ecore_file_realpath(argv[2]);

   dbs_load();

   if (ipc_init())
     {
        ecore_exe_run_priority_set(10);
        ecore_event_handler_add(ECORE_EXE_EVENT_DEL, _exe_cb_del, NULL);
        ecore_main_loop_begin();
     }

   ecore_ipc_shutdown();
   ecore_file_shutdown();
   eet_shutdown();
   ecore_shutdown();
   eina_shutdown();
   return 0;
}
