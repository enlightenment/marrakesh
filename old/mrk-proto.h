#define M_UP_START   1
#define M_UP_DATA    2
#define M_UP_END     3
#define M_UP_OK      4
#define M_UP_FAIL    5

#define M_DOWN_START 11
#define M_DOWN_DATA  12
#define M_DOWN_END   13

#define M_QRY_LIST   21
#define M_QRY_SEARCH 22
#define M_QRY_GET    23
#define M_QRY_GETSRC 24

#define M_ANS_START  31
#define M_ANS_DATA   32
#define M_ANS_END    33

#define M_SRC_START  41
#define M_SRC_DATA   42
#define M_SRC_END    43

#define M_ID_UUID    51
#define M_ID_VERSION 52
#define M_ID_ARCH    53

#if      defined(__x86_64__)
# define ARCH "x86_64"
#elif    defined(__i386__)
# define ARCH "ix86"
#elif    defined(__arm__)
# define ARCH "arm"
#elif    defined(__ppc64__) || defined(__powerpc64__)
# define ARCH "ppc64"
#elif    defined(__ppc__) || defined(__powerpc__)
# define ARCH "ppc"
#endif

#if      defined(__linux__)
# define OS "linux"
#elif    defined(__MACH__)
# define OS "osx"
#elif    defined(__FreeBSD__)
# define OS "freebsd"
#elif    defined(_WIN32) || defined(_WIN64)
# define OS "win"
#endif
