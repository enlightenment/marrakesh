#include "mrklib_priv.h"

static const char *sane_name_veto[] = 
{"../", "./", "/", NULL};
static const char *sane_name_ok = 
"01234567890-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ. ";
static const char *sane_path_veto[] = 
{"../", "./", NULL};
static const char *sane_path_ok = 
"/01234567890-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ. ";

static int
_sane_forbidden_path(const char *file, const char **forbidden)
{
   int i;

   for (i = 0; forbidden[i]; i++)
     {
        if (strstr(forbidden[i], file)) return 0;
     }
   return 1;
}

static int
_sane_allowed_path(const char *file, const char *allowed)
{
   int i, j;

   for (i = 0; file[i]; i++)
     {
        int ok;

        ok = 0;
        for (j = 0; allowed[j]; j++)
          {
             if (file[i] == allowed[j])
               {
                  ok = 1;
                  break;
               }
          }
        if (!ok) return 0;
     }
   return 1;
}

Eina_Bool
_mrk_util_plain_file_check(const char *file)
{
   if (_sane_forbidden_path(file, sane_name_veto) &&
       _sane_allowed_path(file, sane_name_ok))
     return EINA_TRUE;
   return EINA_FALSE;
}

Eina_Bool
_mrk_util_plain_path_check(const char *file)
{
   if (_sane_forbidden_path(file, sane_path_veto) &&
       _sane_allowed_path(file, sane_path_ok))
     return EINA_TRUE;
   return EINA_FALSE;
}

char *
_mrk_util_proto_string(Ecore_Ipc_Event_Server_Data *e)
{
   char *str = malloc(e->size + 1);
   if (!str) return NULL;
   memcpy(str, e->data, e->size);
   str[e->size] = 0;
   return str;
}

char *
_mrk_util_proto_cli_string(Ecore_Ipc_Event_Client_Data *e)
{
   char *str = malloc(e->size + 1);
   if (!str) return NULL;
   memcpy(str, e->data, e->size);
   str[e->size] = 0;
   return str;
}

Eina_Bool
_mrk_util_arch_ok(const char *arch)
{
   if (!strcmp(arch, "linux-x86_64")) return EINA_TRUE;
   if (!strcmp(arch, "linux-ix86")) return EINA_TRUE;
   if (!strcmp(arch, "linux-armv7")) return EINA_TRUE;
   if (!strcmp(arch, "linux-armv6")) return EINA_TRUE;
   if (!strcmp(arch, "linux-ppc64")) return EINA_TRUE;
   if (!strcmp(arch, "linux-ppc")) return EINA_TRUE;
   return EINA_FALSE;
}
