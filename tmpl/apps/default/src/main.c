#include "main.h"

static void
_cb_button_clicked(void *data, Evas_Object *obj EINA_UNUSED , void *info EINA_UNUSED)
{
   Evas_Object *win = data;

   evas_object_del(win);
}

EAPI_MAIN int
elm_main(int argc EINA_UNUSED, char **argv EINA_UNUSED)
{
   Evas_Object *win, *bg, *bx, *lb, *bt;
   char buf[PATH_MAX];

   elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
   elm_app_compile_bin_dir_set(PACKAGE_BIN_DIR);
   elm_app_compile_lib_dir_set(PACKAGE_LIB_DIR);
   elm_app_compile_data_dir_set(PACKAGE_DATA_DIR);
   elm_app_info_set(elm_main, "@APPDOM@", "README");

   win = elm_win_add(NULL, "@APPNAME@", ELM_WIN_BASIC);
   if (!win) return -1;
   elm_win_title_set(win, "@APPNAME@");
   elm_win_autodel_set(win, EINA_TRUE);

   bg = elm_bg_add(win);
   evas_object_size_hint_weight_set(bg, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   snprintf(buf, sizeof(buf), "%s/bg/bg.jpg", elm_app_data_dir_get());
   elm_bg_file_set(bg, buf, NULL);
   elm_win_resize_object_add(win, bg);
   evas_object_show(bg);

   bx = elm_box_add(win);
   evas_object_size_hint_weight_set(bx, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_win_resize_object_add(win, bx);
   evas_object_show(bx);

   lb = elm_label_add(win);
   elm_label_line_wrap_set(lb, ELM_WRAP_WORD);
   elm_object_text_set(lb,
                       "<b>Hello World</b><br>"
                       "<br>"
                       "This is my first EFL App! I hope it's good"
                      );
   evas_object_size_hint_weight_set(lb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(lb, EVAS_HINT_FILL, 0.5);
   elm_box_pack_end(bx, lb);
   evas_object_show(lb);

   bt = elm_button_add(win);
   elm_object_text_set(bt, "Close Window");
   evas_object_smart_callback_add(bt, "clicked", _cb_button_clicked, win);
   evas_object_size_hint_fill_set(bt, EVAS_HINT_FILL, 0.0);
   evas_object_size_hint_weight_set(bt, EVAS_HINT_EXPAND, 0.0);
   elm_box_pack_end(bx, bt);
   evas_object_show(bt);

   evas_object_show(win);

   elm_run();
   return 0;
}
ELM_MAIN()
