#!/bin/sh
# build dir dstdir
set -e

cd "$1"
rm -f *.mkb || true
rm -rf Marrakesh || true

ls
mrk extract *.mks

export CC=gcc
export CFLAGS="-O3 -ffast-math"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig"
export NAME=`grep "^PROJ:" Marrakesh.mrk | tail -1 | sed -e 's/PROJ://g' | sed -e 's/ *$//' -e 's/^ *//'`
export VERSION=`grep "^VERSION:" Marrakesh.mrk | tail -1 | sed -e 's/VERSION://g' | sed -e 's/ *$//' -e 's/^ *//'`

## this is a sample - you need to do this N times, once per arch
export MRKARCH="x86_64"
export MRKOS="linux"
mrk build
mrk bin
mrk src
export D="$2"/"$MRKOS-$MRKARCH"
mkdir -p "$D" || true
cp "$NAME"-"$VERSION".mkb "$D"
pushd "$D"
rm -f "$NAME" || true
ln -s "$NAME"-"$VERSION".mkb "$NAME"
popd

## # copy src to right place
export D="$2"/"src"
mkdir -p "$D" || true
cp "$NAME"-"$VERSION".mks "$D"
pushd "$D"
rm -f "$NAME" || true
ln -s "$NAME"-"$VERSION".mks "$NAME"
popd

cd /tmp
rm -rf "$1"

exit 0
